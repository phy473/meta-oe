SUMMARY = "Message Passing Interface (MPI) implementation - process manager"
HOMEPAGE = "http://www.mpich.org/"
SECTION = "devel"

LICENSE = "BSD-2-Clause"
LIC_FILES_CHKSUM = "file://tools/topo/hwloc/hwloc/COPYING;md5=66a5f6f418ecccec4724869497edc415"

SRC_URI = " \
    http://www.mpich.org/static/downloads/${PV}/hydra-${PV}.tar.gz \
"

SRC_URI[md5sum] = "86f6b73ba9008fafbd52b02c34032edb"
SRC_URI[sha256sum] = "47a267974f7707b86537c95c1c81ba9961b963d8900631e50b30bc492f4bf587"

RDEPENDS_${PN} += "bash perl libxml2 mpich"
S = "${WORKDIR}/${BP}"

EXTRA_OECONF = "BASH_SHELL='${USRBINPATH}/env bash' \
    PERL='${USRBINPATH}/env perl' \
"

inherit autotools-brokensep gettext

do_configure_prepend() {
    autoreconf --verbose --install --force -I . -I confdb/ -I tools/topo/hwloc/hwloc/config/
    oe_runconf
    exit
}

do_install_append() {

}
